const express = require('express');
const bodyParser = require('body-parser');


const app = express();

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

const routes = require('./router/routes');
app.use(routes);

const query = require('./models/table');
query.category();
query.quetionTable();
query.answerTable();


app.listen(4000,()=>console.log("port running on 4400"));