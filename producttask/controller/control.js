const connection = require('../dataBase/db');

addCategory = (req,res)=>{
    console.log(req.body);
    let cat = [
        req.body.category,
        req.body.parent_id
    ];
    let addcat = `INSERT INTO category(category,parent_id) VALUES(?,?)`;
    connection.query(addcat,cat,function(error,rows,field){
        if(error){
            console.log(rows);
            console.log("error=>>",error)
            res.send({succes:false})
        }else{
            res.send({succes:true})
            console.log("answer added ...")
        }
    })
}

getCategories = (req,res)=>{
    let getcat = `SELECT * FROM category`;
    connection.query(getcat,function(error,rows){
        if(error){
            res.send({succes:false});
        }else{
            console.log("categories=>",rows)
            res.send({data:rows})
        }
    })
}

addQuetion = (req,res)=>{
    console.log(req.body);
    let quetion = [
        req.body.question,
        req.body.parent_id
    ];
    console.log(quetion);
    let addQuestion = `INSERT INTO quetions(questions,cat_id) VALUES(?,?)`;
    connection.query(addQuestion,quetion,function(error,rows,field){
        if(error){
            console.log("error=>>",error)
            res.send({succes:false})
        }else{
            res.send({succes:true})
            console.log("question added ...")
        }
    })
}

getQuestion = (req,res)=>{
    let  getQues = `SELECT * FROM quetions`;
    connection.query(getQues,function(error,rows){
        if(error){
            console.log("error=>>",error)
            res.send({succes:false})
        }else{
            console.log("quetions=>",rows);
            res.send({Data:rows})
        }
    })    
}



module.exports = {
    categoryAdd : addCategory,
    categoryGet : getCategories,
    questionAdd : addQuetion,
    questionGet : getQuestion   
}