const express = require('express');
const routes = express.Router();
const addData = require('../controller/control')

routes.post('/addcategory',addData.categoryAdd);

router.get('/getcategories',addData.categoryGet);

router.post('/addquestion',addData.questionAdd);

router.get('/getquetions',addData.questionGet);

module.exports = routes;
